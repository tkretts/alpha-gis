# Alpha-GIS #

**alpha-gis** - модуль для интеграции системы на базе платформы m3 2.0 с геоинформационной системой платформы бизнес-аналитики БАРС.Альфа-BI

## Описание ##
Модуль предназначен для размещения объектов системы на карте и их последующего отображения на общей карте, с возможностью отображения также дополнительной информации по ним.
Работа с модулем требует взаимодействия с разработчиками платформы БАРС.Альфа-BI, поскольку с их стороны остаются необходимыми некоторые настройки, такие как создание нового слоя для новых объектов, выделение учетных данных и идентификаторов действий API, и пр.

## Установка и настройка ##
1. Скачать [последнюю версию модуля](https://bitbucket.org/tkretts/alpha-gis/downloads/alpha-gis-1.0.4.tar.gz) или необходимую в разделе ["Загрузки"](https://bitbucket.org/tkretts/alpha-gis/downloads).
2. Установить модуль в окружение проекта:
```
#!python
pip install alpha-gis-x.x.x.tar.gz
```
3. Подключить модуль в settings.py проекта:
```
#!python
INSTALLED_APPS = (
    ...
    'alpha_gis', 
    ...
)
```
4. Указать в настройках проекта или конфигурационном файле адреса API Yandex.Maps и БАРС.Альфа-BI:
```
#!python
YANDEX_MAP_API_URL = 'http://geocode-maps.yandex.ru/1.x'
ALPHA_API_URL = 'http://server/monitoring/ur-user/'
```
5. Указать в настройках проекта или конфигурационном файле необходимые для взаимодействия настройки:
```
#!python
## BARS.Alpha-BI GIS Integration settings
ALPHA_USER: admin  # Имя пользователя, выделенное для интеграции со стороны БАРС.Альфа-BI
ALPHA_PASS: entry  # Пароль пользователя

ALPHA_CONFIG:  # Настройки взаимодействия приложений
  ROOMS:  # Идентификатор приложения. В последствии определяется в качестве свойства у ActionPack'а
    LAYER: 'Животноводческие помещения'  # Наименование слоя, выделенное со стороны БАРС.Альфа-BI
    TYPE_ID: ZverFarm  # Идентификатор типа, также выдается БАРС.Альфа-BI
    MAP_ZOOM: 7  # Масштаб карты по умолчанию. Принимает значение целого типа от 1 до 16, где 16 - самый крупный
    ONE_ACTION_ID: 124618  # Идентификатор действия размещения одного объекта указанного приложения. Выделяется БАРС.Альфа-BI
    ALL_ACTION_ID: 108541  # Идентификатор действия отображения всех размещенных объектов приложения. Выделяется БАРС.Альфа-BI
    REFRESH_CONFIG:  # Конфигурация действия загрузки дополнительной информации по объектам. Выделяется БАРС.Альфа-BI
      PIPELINE: 124696  # Идентификатор действия. Выделяется БАРС.Альфа-BI.
```
6. Подключить в базовом шаблоне статику:
```
#!html
    <script type="text/javascript" src="{{ STATIC_PREFIX }}js/alpha_gis.js"></script>
```
7. Если сервер приложения имеет доступ в Интернет через прокси, для взаимодействия с Yandex.Maps API можно также указать настройки прокси:
```
#!python
PROXY_SETTINGS:
  http: http://user:pass@proxy:8080/
  https: https://user:pass@proxy:8080/
``` 
## Использование ##
* В первую очередь, для главного ActionPack'а интегрируемого приложения, необходимо подключить миксин **AlphaActionPackMixin**, импортируемый из модуля **alpha-gis**, а также указать текстовый идентификатор конфигурации:
```
#!python
class RoomsActionPack(AlphaActionPackMixin, ActionPack):
    ...
    alpha_config_name = 'ROOMS'
    ...
```
При этом, ActionPack получает дополнительные экшны обновления дополнительной информации и отображения всех объектов на карте. Для использования данных действий необходимо только настроить возможность их вызова удобным способо
Например, в качестве кнопки на главной панели грида приложения:
```
#!python
action_pack = ControllerCache.find_pack(
    "path.to.app.RoomsActionPack")
self.alpha_url = (
    action_pack.alpha_all_on_map_action.get_absolute_url())
self.insert_menu_item_grid(
    text=_(u'Карта'),
    icon_cls=Icons.MAP,
    handler="showAllRoomsOnMap",
    position=4,
)
```
```
#!javascript
function showAllRoomsOnMap(){
    var roomsOnMapUrl = '{{ component.alpha_url }}';
    doAction(roomsOnMapUrl, win.actionContextJson, function(res, opt){
        smart_eval(res.responseText);
    });
}
```
Или в качестве кнопки в меню "Пуск":
```
#!python
def register_desktop_menu():
    admin_group = DesktopLaunchGroup(
        name=_(u"Администрирование"),
        icon="menu-dicts-16"
    )
    alpha_group = DesktopLaunchGroup(
        name=_(u"БАРС.Alpha-BI интеграция"),
        icon='icon-map',
    )
    rooms_pack = ControllerCache.find_pack(actions.RoomsActionPack)
    refresh_rooms_action = rooms_pack.alpha_refresh_action
    alpha_group.subitems.append(
        DesktopLauncher(
            name=_(u'Обновить "Животноводческие помещения"'),
            url=refresh_rooms_action.get_absolute_url(),
            icon='sync',
        )
    )
    admin_group.subitems.append(alpha_group)
    DesktopLoader.add(ADMIN, DesktopLoader.START_MENU, admin_group)
```
### Размещение объектов на карте ###
Для размещения объектов на карте используются следующие встроенные компоненты:

* Класс **YandexGeocode** принимает в качестве параметра при инициализации строку с адресом объекта, по которому определяет координаты объекта. Получить доступ к координатам объекта можно через свойства **point** (возвращает две координаты, x и y) и **rectangle** (возвращает координаты x;y двух точек, верхнего левого и нижнего правого углов прямоугольника):
```
#!python
address_string = (
    getattr(context, 'address_string', '') or room.uaddr)
api_obj = YandexGeocode(address_string)
obj_point = api_obj.point
obj_rect = api_obj.rectangle
```
* Компоненты интерфейса **ExtSimpleComponent** и **ExtIFrameComponent** предназначены непосредственно для отображения карты. В случае с общей картой они подключаются автоматически. 
Для подключения окна размещения объкта на карте, необходимо разместить **ExtIFrameComponent** внутри **ExtSimpleComponent**, а в дальнейшем только менять ему свойство *src* на необходимый URL GET-запроса:
```
#!python
self.gis_iframe = ExtIFrameComponent(
    src="", style={'width': '99%', "height": "98%"}
)
gis_panel = ExtSimpleComponent(auto_el=self.gis_iframe)
gis_tab.items.append(gis_panel)
```
* JS-функции **obj2str** и **doAlphaAction**:
**obj2str** - обычный хелпер, позвояет преобразовать полученный объект в строку параметров для GET-запроса;
**doAlphaAction** - выполняет авторизацию на сервере платформы БАРС.Альфа-BI и передает токен сессии callback-функции:
```
#!javascript
function showAllOnMap(){
    var alphaFrame = document.getElementById(
            '{{ component.alpha_iframe.client_id }}'),
        alphaUrl = '{{ component.alpha_api_url }}',
        authObj = {
            'login': '{{ component.alpha_login }}',
            'password': '{{ component.alpha_password }}'
        },
        allActionId = '{{ component.all_action_id }}';

    doAlphaAction(alphaUrl + 'login', authObj, function(alphaToken){
        alphaFrame.src = alphaUrl + '?OpenAction=' + allActionId +
            '&AuthToken=' + alphaToken + '&' + obj2str(roomPointParams);
    });
}
```