# coding: utf-8
__author__ = 'tkretts'

from django.conf import settings
from django.utils.translation import ugettext as _

from m3_ext.ui.windows.base import BaseExtWindow
from m3_ext.ui.base import ExtUIComponent
from m3_ext.ui.containers import ExtPanel


class ExtSimpleComponent(ExtUIComponent):
    """ Простой компонент ExtJS с xtype: 'component'
        По сути, контейнер, необходимый для размещения iframe'а
    """

    def __init__(self, auto_el=None, *args, **kwargs):
        super(ExtSimpleComponent, self).__init__(*args, **kwargs)

        if isinstance(auto_el, basestring):
            self.auto_el = auto_el
        elif isinstance(auto_el, ExtUIComponent):
            self.auto_el = auto_el.render()

        self.init_component(*args, **kwargs)

    def render(self):
        res = ['id:"{0}"'.format(self.client_id), "xtype: 'component'"]
        if self.auto_el:
            res.append("autoEl: {0}".format(self.auto_el))
        return "{{{0}}}".format(','.join(res))

    def _make_read_only(self, access_off=True, exclude_list=(), *args,
                        **kwargs):
        self.disabled = access_off


class ExtIFrameComponent(ExtUIComponent):
    """ IFrame-компонент
    """

    def __init__(self, src="", style=None, *args, **kwargs):
        super(ExtIFrameComponent, self).__init__(*args, **kwargs)

        self.src = src
        self.style = style or {}

        self.init_component(*args, **kwargs)

    def render(self):
        """ render component
        """
        res = ['id:"{0}"'.format(self.client_id), "tag: 'iframe'"]
        if self.src:
            res.append("src: '{0}'".format(self.src))
        if self.style:
            res.append("style: {0}".format(str(self.style)))
        res.append("autoScroll: false")
        return "{{{0}}}".format(','.join(res))

    def _make_read_only(
            self, access_off=True, exclude_list=(), *args, **kwargs):
        self.disabled = access_off


class BaseAllPointsWindow(BaseExtWindow):
    """ Окно отображения всех точек приложения на карте
    """

    def __init__(self):
        super(BaseAllPointsWindow, self).__init__()
        self.template_globals = 'ui-js/all-on-map-window.js'
        self.title = _(u'Карта')
        self.maximized = True
        self._init_components()
        self._do_layout()

    def _init_components(self):
        self.main_panel = ExtPanel(
            body_cls="x-window-mc",
            title=_(u"Карта"),
            layout="form",
            anchor="100%",
        )

        self.alpha_api_url = getattr(settings, 'ALPHA_API_URL', '')

        self.alpha_iframe = ExtIFrameComponent(
            src="", style={'width': '99%', "height": "98%"}
        )
        gis_panel = ExtSimpleComponent(auto_el=self.alpha_iframe)
        self.main_panel.items.append(gis_panel)

    def _do_layout(self):
        self.layout = 'fit'
        self.items.append(self.main_panel)
