# coding: utf-8
__author__ = 'tkretts'

import re
import urlparse
import urllib2
from django.conf import settings

from lxml import html


def url_encode_non_ascii(b):
    return re.sub('[\x80-\xFF]', lambda c: '%%%02x' % ord(c.group(0)), b)


def iri_to_uri(iri):
    parts = urlparse.urlparse(iri)
    return urlparse.urlunparse(
        part.encode('idna') if parti == 1 else url_encode_non_ascii(
            part.encode('utf-8')
        ) for parti, part in enumerate(parts)
    )


class YandexGeocode(object):
    """ Yandex.Maps.API geocode object
    """
    api_url = ""
    xml_res = None

    def __init__(self, address_string, api_url=""):
        """ @param basestring address_string: Custom address string
            @param basestring api_url: Yandex.Maps API URL
        """
        self.api_url = api_url or settings.YANDEX_MAP_API_URL
        self.get_xml_res(address_string)

    def get_xml_res(self, addr_str=''):
        """ Gets xml from Yandex.Maps.API
            @param basestring addr_str: Custom address string
        """
        try:
            if hasattr(settings, 'PROXY_SETTINGS'):
                proxy = urllib2.ProxyHandler(settings.PROXY_SETTINGS)
                opener = urllib2.build_opener(proxy)
                urllib2.install_opener(opener)
            req = urllib2.urlopen(iri_to_uri(u'{0}?geocode={1}'.format(
                self.api_url, addr_str)))
            if req.code == 200:
                self.xml_res = html.parse(req).getroot()
        except urllib2.URLError:
            self.xml_res = html.etree.Element("ymap")

    @property
    def point(self):
        """ Get point coordinates
            @return dict: coordinates x, y
        """
        res = {'x': .0, 'y': .0}
        points = self.xml_res.xpath('//point/pos')
        try:
            coords = map(float, points[0].text.split())
            res['x'], res['y'] = coords
        except (IndexError, ValueError):
            pass

        return res

    @property
    def rectangle(self):
        """ Get rectangle's coordinates
            @return dict: coordinates ((x1, y1), (x2, y2))
        """
        res = {'x1': .0, 'x2': .0, 'y1': .0, 'y2': .0}
        rectangle = self.xml_res.xpath("//envelope/*")
        try:
            lower_corner, upper_corner = rectangle
            res['x1'], res['y1'] = map(float, lower_corner.text.split())
            res['x2'], res['y2'] = map(float, upper_corner.text.split())
        except (IndexError, ValueError):
            pass

        return res